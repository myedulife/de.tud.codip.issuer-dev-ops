# Install Keycloak

Install keycloak locally on your system with docker. 

```
docker pull jboss/keycloak:16.1.1
docker run -p 8080:8080 jboss/keycloak:16.1.1
docker exec <CONTAINER> /opt/jboss/keycloak/bin/add-user-keycloak.sh -u <USERNAME> -p <PASSWORD>
docker restart <CONTAINER>
```

